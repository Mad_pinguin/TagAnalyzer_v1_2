# TagAnalyzer_v1_2

To start ngrok use "./ngrok http 8000" in folder "/home/pavel/TagAnalyzer-main/Django_server/",
then  copy and paste https url into android app in ServiseGenerator class.
Also paste this url in settings.py without "https://".

To run server use "python3 manage.py runserver" in same folder

Для запуска нгрока  в директории "/home/pavel/TagAnalyzer-main/Django_server/" используется команда "./ngrok http 8000",
Затем ссылка по hhtps вставляется целиком в приложение (класс ServiseGenerator), а для самого сервера в файле settings.py  (без https).

Для запуска сервера в тойже директории выполняется команда "python3 manage.py runserver"
